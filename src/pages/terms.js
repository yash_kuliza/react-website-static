import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Header from "../components/header";
import NavItems from "../components/nav-items";
import MobileNav from "../components/mobile-menu";
import Footer from "../components/footer";
import BottomNav from "../components/bottom-nav";
import NavIcons from "../components/nav-icons";

const TNC = () => (
  <Layout>
    <SEO title="Terms and Conditions" />
    <div className="homepage">
      <div className="header">
        <Header />
        <NavItems />
        <MobileNav />
      </div>
    </div>
    <div className="homepage tnc">
      <h1>Mega App Terms and Conditions</h1>
      <p>
        Thank you, dear player, for wanting to play at Mega. By now you would
        have downloaded the “Mega App” and installed it.{" "}
      </p>
      <p>
        Before you create your own “Player Profile”, we encourage you to read
        these “Terms and Conditions” (“Terms”). To make it easier for you we
        have broken down the Terms into easy-to-read bits. When you hit the
        ‘Accept’ button at the end of the Terms, you are deemed to have read and
        understood the Terms. The Terms are a binding contract between you and
        Megashots Internet Private Limited, a company incorporated under the
        Companies Act, 2013 and having its registered office at Indiqube Orion
        that owns the Mega App (“Mega” or “our” or “we”). If you do not want to
        accept the Terms, make sure you do not hit the ‘Accept’ button. These
        Terms may be changed or substituted altogether at any time at our sole
        discretion and such changes or substitutions shall be binding on you
        from the date they are published.
      </p>
      <h3>A Quick Tour</h3>
      <p>
        Here is a quick tour of how you will play at Mega. Besides helping you
        understand Mega, we hope the tour will also help you appreciate the
        Terms. Download Mega App → Install Mega App → Accept these Terms →
        Create a Player Profile → Recharge Mega Wallet → Buy a Pack → Choose a
        Game → Choose a Contest → Get a High Score → Win money → Get your
        winnings credited to the Mega Wallet after taxes → Withdraw your Mega
        Wallet balance to your personal bank account
      </p>
      <h3>Eligibility</h3>
      <p>
        If you are less than 18 years of age, you are legally ineligible to play
        on the Mega App. If you falsely claim to be of age, then we are not
        liable in any way, either to you or your parents/guardians. If you are
        over 18 years of age, you are deemed to have consented to every action
        taken by any person who accesses the Mega App, including your children
        or friends. Please note we shall be entitled to presume that any person
        who logs in with your login details is you. In your interest, we
        recommend you keep your login details confidential. We shall not be
        responsible if any person other than you logs in and plays at Mega or
        takes any action on the Mega App using your login details.
      </p>
      <h3>Minimum Technical Requirements</h3>
      <p>
        In order to ensure optimal use of the Mega App, you will need to access
        the App on an android phone with a RAM of at least 2 GB and a 4G
        internet connection. We will not be responsible for sub-optimal results
        because of device and internet connections.
      </p>
      <h3>Territory</h3>
      <p>
        Games on the Mega App are skill-based games and do not in any way amount
        to betting or gambling. Some states in India may prohibit any kind of
        online gaming, including predominantly skill based games like the Games
        on Mega. Some other states in India may prohibit any online games that
        are not predominantly skill based. In any event, Mega shall make best
        efforts to not make the Mega App available in any state of India to the
        extent such jurisdictions restrict or prohibit games like the Games on
        Mega. In any event, it shall be solely your responsibility to ensure
        that the local laws of the state in which you attempt to access the Mega
        App allows you to play the Games. We hereby disclaim all responsibility
        in the event you unlawfully access the Mega App, the Games and any other
        features therein.{" "}
      </p>
      <h3>Player Profile and Privacy</h3>
      <p>
        After you accept these Terms you will be required to enter some personal
        information on the Mega App and create your profile. All personal
        information will be handled by Mega subject to the terms and conditions
        of Mega’s “Privacy Policy”.
      </p>
      <h3>Packs</h3>
      <p>
        After you create your Player Profile, you are ready to buy a “Pack”
        which gives you access to a certain number of contests on Mega as
        defined on the “Packs” Page. Packs on Mega are timebound. Currently you
        can buy a 1-day Pack, or a 1-week Pack or a 1-month Pack. You need to
        pay the prescribed non-refundable “Fee” in Indian Rupees to buy a Pack.
        Read on to learn how you can do that.
      </p>
      <h3>Wallets on Mega</h3>
      <p>Mega has three kinds of Wallets:</p>
      <h4>Mega Money Wallet:</h4>{" "}
      <p>
        This is a closed wallet where credits happen in two ways: (a) when you
        recharge this wallet with real money transferred from your designated
        personal bank account; (b) when Mega credits this wallet with your
        Winnings. You can use the balance in your Mega Money Wallet to buy Packs
        or you can withdraw the balance outstanding at any time into the
        designated bank account.
      </p>
      <h4>Mega Credits Wallet:</h4>{" "}
      <p>
        This wallet contains credits you earn as rewards under various
        promotional schemes at Mega. The credits in your Mega Credits Wallet can
        be used only to buy Packs. You cannot withdraw credits into your
        designated bank account.
      </p>
      <h4>Mega Coins Wallet:</h4>{" "}
      <p>
        This wallet contains no real money. It contains only ‘Mega Coins’ you
        pick up along the way while playing Games. If you play multiple sessions
        of the same Game as allowed by the Pack you have purchased, the coins
        you pick up will be determined by your High Score as defined below. You
        can use Mega Coins only to make in-Game purchases like superpowers and
        not for anything else. Remember, Mega Coins are not real money.
      </p>
      <h3>Games, Contests and Winnings</h3>
      <p>
        Once you have purchased a Pack using either Mega Money or Mega Credits,
        you are ready to choose and play one of several ‘Games’ at Mega. Each
        Game may have one or more virtual game rooms which we call “Contests”.
        In order to play a Game, you must participate in a Contest; otherwise
        you cannot play the Game. Contests have a fixed duration. You can play
        multiple “Game Sessions” of the Game in the same Contest until either
        the Contest period gets over or the duration of your Pack gets over,
        whichever is earlier. Each time you play a Game Session, you will make a
        score. During the validity of a Contest, your highest score across Game
        Sessions will be logged as your “High Score”. Your High Score as well as
        every other player’s High Score will be displayed after each Game
        Session. To help you decide which Contest to enter, Mega will present
        key information about each Contest including the maximum amount of money
        available for distribution to players depending on their placing on the
        leaderboard (“Winnings”). Whether your High Score will result in
        Winnings will depend on the rules of the particular Contest. At any
        rate, Winnings will be determined and declared for each Contest only
        after the validity of that Contest ends. Winnings will be credited to
        your Mega Money Wallet as soon as reasonably possible after the end of
        the relevant contest. Thereafter, you can transfer your Winnings to your
        designated bank account at any time.
      </p>
      <h3>Disclaimers and Limitation of Liability</h3>
      <p>
        Mega does not represent, warrant, promise or assure you of anything,
        including by way of making any promise that the Mega App or any Game or
        Contest will be available all the time, or that you will make any
        Winnings, or that the Games and Contests will be free from any defects
        or will be suited for any particular purpose. Without limitation, Mega
        will not be responsible for any delay or failure resulting from
        infrastructure issues, such as server uptime and network availability.
        All representations, warranties and indemnities, express or implied, are
        expressly disclaimed.
      </p>
      <p>
        The Games and Contests are controlled and offered by Mega from its
        facilities in India. If you are a player outside India, please note Mega
        is subject only to Indian law. Therefore, you may use play the Games and
        enter into the Contests solely at your own risk.{" "}
      </p>
      <p>
        Any information that the Mega App may communicate to you about any
        service or product offered by a third party, including any promotional
        or other offers, shall not constitute or be deemed to be Mega’s
        endorsement or approval in any manner of such third-party products and
        services.
      </p>
      <p>
        You agree that if you use or otherwise obtain any third-party content
        through the Mega App, the same shall be at your own discretion and risk
        and that you will be solely responsible for any damage to your property
        (including your mobile phone and/or other device) that results from the
        use of such content.
      </p>
      <p>
        You hereby agree to indemnify Mega and its officers, directors,
        employees, consultants, vendors, suppliers and agents against any and
        all claims, demands, expenses, charges, costs, damages, losses,
        obligations or liabilities by whatever name called, made against or
        suffered by any of the foregoing persons as a result of your acts and/or
        omissions.{" "}
      </p>
      <p>
        Mega will not be liable to you for any special indirect, consequential
        or punitive damages, arising out of or relating to these Terms,
        including for any reason that you are unable to use the Mega App, or as
        a result of any interactions with another user on the Mega App, or
        violation of any restriction or prohibition on the Mega App or any one
        or more Games or Contests. If the foregoing limitation is not
        enforceable, the maximum liability of Mega shall be limited to the price
        you have paid for the Pack in respect of which you have a claim.
      </p>
      <h3>Termination</h3>
      <p>
        Mega may discontinue, block or restrict the Mega App and/or your access
        to the Mega App at any time with or without notice, and for or without
        any reasons, with absolutely no liability to you.
      </p>
      <p>
        Mega may terminate these Terms without notice and without liability to
        you on becoming aware that you have violated these Terms.
      </p>
      <p>
        Mega may suspend or altogether stop any Contest, or Game or the Mega
        App, with no termination charges or other damages payable to you as a
        result thereof.
      </p>
      <h3>Intellectual Property</h3>
      <p>
        Mega and its licensors, if any, are the sole and absolute owners of the
        Mega App, including but not limited to the copyright in all game design,
        content and all trademarks, designs, logos and other insignia of trade
        used on the Mega App. No license or other permission is granted to you
        in respect of the Mega App except a limited permission to validly use
        the Mega App in accordance with these Terms.{" "}
      </p>
      <h3>Governing Law</h3>
      <p>
        These Terms are governed by and shall be construed in accordance with
        the laws of India.{" "}
      </p>
      <p>
        Mega will coordinate with you to attempt to amicably resolve disputes,
        if any, for a period of thirty (30) days. Unresolved disputes will be
        referred to a sole arbitrator in Bangalore appointed by Mega in
        accordance with the Arbitration and Conciliation Act, 1996. Subject to
        the foregoing, you hereby consent to the exclusive jurisdiction of the
        Courts in Bangalore.
      </p>
      <h3>Miscellaneous</h3>
      <p>
        Any notice required to be given to Mega under this Agreement will be
        sent by registered mail or recognized courier to the address set out
        above, with an electronic copy of the same marked to{" "}
        <a href="mailto:help@getmega.com">help@getmega.com</a> If any part of
        any provision of these Terms may prove to be illegal or unenforceable,
        the other provisions of these Terms shall continue in full force and
        effect. No failure or delay in exercising any right, power or privilege
        hereunder shall operate as a waiver thereof. The right to use the Mega
        App is personal to you and is not transferable by assignment,
        sublicense, or any other method to any other person or entity.
      </p>
      <BottomNav />
      <NavIcons />
      <Footer />
    </div>
  </Layout>
);

export default TNC;
