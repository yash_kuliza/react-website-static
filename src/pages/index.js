import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Home from "../components/home";
// import HowToInstall from "../components/how-to-install";
// import AboutMega from "../components/about-mega";
import Footer from "../components/footer";
// import DownloadApp from "../components/download-app";
import Features from "../components/features";
import BottomNav from "../components/bottom-nav";
import NavIcons from "../components/nav-icons";

const IndexPage = () => (
  <>
    <Layout>
      <SEO title="Mega" keywords={[`mega`, `play games`, `earn money`]} />
      <Home />
    </Layout>
    <Layout>
      <SEO
        title="Mega"
        keywords={[`mega`, `play games`, `earn money`, `features`, `loot`]}
      />
      <Features />
    </Layout>
    <Layout>
      <SEO
        title="Mega"
        keywords={[`mega`, `play games`, `earn money`, `features`, `loot`]}
      />
      <BottomNav />
    </Layout>
    <Layout>
      <SEO
        title="Mega"
        keywords={[`mega`, `play games`, `earn money`, `features`, `loot`]}
      />
      <NavIcons />
    </Layout>
    {/* <Layout>
      <SEO
        title="Mega"
        keywords={[`mega`, `play games`, `earn money`, `install`, `download`]}
      />
      <HowToInstall />
    </Layout> */}
    {/* <Layout>
      <SEO
        title="Mega"
        keywords={[
          `mega`,
          `play games`,
          `earn money`,
          `install`,
          `download`,
          `about`
        ]}
      />
      <AboutMega />
    </Layout>
    <Layout>
      <SEO
        title="Mega"
        keywords={[
          `mega`,
          `play games`,
          `earn money`,
          `install`,
          `download`,
          `about`,
          `download`
        ]}
      />
      <DownloadApp />
    </Layout>*/}
    <Layout>
      <Footer />
    </Layout>
  </>
);

export default IndexPage;
