import React from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Header from "../components/header";
import NavItems from "../components/nav-items";
import MobileNav from "../components/mobile-menu";
import Footer from "../components/footer";
import BottomNav from "../components/bottom-nav";
import NavIcons from "../components/nav-icons";

const PrivacyPolicy = () => (
  <Layout>
    <SEO title="Privacy Policy" />
    <div className="homepage">
      <div className="header">
        <Header />
        <NavItems />
        <MobileNav />
      </div>
    </div>
    <div className="homepage policy">
      <h1>INTRODUCTION</h1>
      <p>
        Welcome to the “Mega App” (“App”, which expression shall include any
        version of the App made available subsequent to the date of this Policy,
        and any companion website owned by the Company), a skill-based, real
        money gaming app owned and maintained by Megashots Internet Private
        Limited (“The Company”) a company registered under the Companies Act,
        2013 and having its registered office at Indiqube Orion and having CIN
        No : U72900KA2018PTC116707.
      </p>
      <h3>1. Purpose of Policy</h3>
      <p>
        This Privacy Policy (“Policy”) has been adopted by the Company in
        accordance with applicable law, and specifically, the Reasonable
        Security Practices and Procedures and Sensitive Personal Data or
        Information Rules, 2011 (“Rules”) under the Information Technology Act,
        2000 (“Act”) of India.
      </p>
      <p>
        The Policy is a statement of the Company’s policies and procedures for
        collection, use, storage, processing, disclosure and protection of any
        Personal Information which you may be required to give or make available
        on the App in order to use the App. “Personal Information”, for the
        purposes of this Policy, includes “Sensitive Personal Information”, and
        these expressions shall have the same meanings as ascribed to them by
        the Rules.
      </p>
      <h3>2. Consent</h3>
      <p>
        It is advisable to go through this Policy carefully before accessing the
        App. This Policy is a legal agreement entered between a user of the App
        (“User” or “you”) and the Company. You are deemed to have accepted this
        Policy by accepting the Terms of Use of the Mega App, or by simply
        downloading and using the App. If you do not agree with this Policy,
        please do not give any Personal Information or use the App.
      </p>
      <p>
        {" "}
        By accepting the Policy as aforesaid, you shall be deemed to have
        consented to the collection, storage, processing and use of your
        Personal Information in accordance with this Policy.
      </p>

      <h3>3. Personal Information </h3>
      <p>
        In order to use the App, you will be required to give, and the App
        collects the following Personal Information and Sensitive Personal
        Information in relation to you:
      </p>
      <ul className="personal-info">
        <li>Name</li>
        <li>Email</li>
        <li>Phone Number</li>
        <li>Facebook Account Id</li>
        <li>Facebook Account Id of Facebook Friends</li>
        <li>Date Of Birth</li>
        <li>Google Account Id</li>
        <li>Facebook Profile Photo</li>
        <li>
          KYC - Picture of Government Issued Photo ID and Address Proof from Any
          of the following Ids (PAN Card, Driving License, Voter Id, Aadhar
          Card, Passport)
        </li>
        <li>KYC - Selfie from Phone</li>
      </ul>
      <p>
        In addition to the above information about you, some data is
        automatically collected i.e., without you being required to disclose it.
        This includes but is not limited to:
      </p>
      <ul className="personal-info">
        <li>Device Details Make and Model</li>
        <li>Network Connectivity (Eg; 2G, 3G, Wifi etc.)</li>
        <li>IP Address</li>
        <li>Approximate Geo-location</li>
      </ul>
      <p>
        We may receive information about you from other sources, including from
        other users and third parties such as advertisers, public social media
        and linked services, if any.
      </p>
      <p>
        We also may receive information about you, including log and usage data
        such as like button clicks, navigation and cookie information, from the
        App and any third-party websites that integrate our Services, including
        our embeds, buttons, and advertising technology.{" "}
      </p>

      <h3>4. Use of the Personal Information</h3>
      <p>
        Personal Information shall be collected, stored, processed and used only
        for the following purposes:
      </p>
      <ul className="personal-info">
        <li>
          To identify you as a legitimate, bona fide, human user of the App;
        </li>
        <li>To provide you with improved services and features on the App;</li>
        <li>To notify you of various aspects of the offerings on the App; </li>
        <li>
          To notify you of offers on products and services, offered by the
          Company or by third parties;
        </li>
        <li>To enable transfer of Winnings to your designated bank account.</li>
        <li>To improve App experience;</li>
        <li>To create a community of users;</li>
        <li>
          Personalize the Services and provide advertisements, content and
          features that match user profiles or interests;
        </li>
        <li>Provide customer service;</li>
        <li>
          Help protect the safety of the Company and our users, which includes
          blocking suspected spammers, addressing abuse, and enforcing the
          Company user agreement and our other policies.
        </li>
      </ul>
      <p>
        Your Personal Information will NOT be used for any purpose other than
        the foregoing purposes. Specifically, the Company will not sell, rent,
        or license your Personal Information to any third party. However, the
        Personal Information may be shared with third party analytics tools in
        an anonymised and encrypted format for the sole purpose of generating
        analytics to improve the App and offer better Services to you.
      </p>
      <h3>5. Data Security</h3>
      <p>
        The Company shall always make best efforts to apply and maintain
        industry standards for the protection of similar information as the
        Personal Information. The data is stored on Google Cloud Platform and in
        encrypted both at rest and in transit. No personally identifiable
        information shall be shared with affiliates or partners.
      </p>
      <h3>6. User’s Rights</h3>
      <p>
        You shall have the right to review your Personal Information submitted
        by you on the App and to modify or delete any Personal Information,
        provided that you hereby understand that any such modification or
        deletion may affect your ability to use the App.
      </p>
      <p>
        On deletion of your Account with us, ALL personally identifiable
        information and Public activity will be deleted.
      </p>
      <h3>7. Applicability of the Terms of Use</h3>
      <p>
        This Policy shall be supplementary to the Terms of Use. Words and
        expressions used in this Policy but not defined herein shall have the
        meanings ascribed to them in the Terms of Use. To the extent any
        provision of this Policy does not conflict with the Terms of Use, the
        Terms of Use shall apply to this Policy, including provisions related to
        Limitation of Liability, Governing Law and Dispute Resolution, and
        Miscellaneous. In the event of any conflict between this Policy and the
        Terms of Use, the interpretation placed by the Company shall be final
        and binding on you.
      </p>
      <h3>Complaints </h3>
      <p>
        For any questions or complaints in relation to any matter concerning
        this Policy, You may contact{" "}
        <a href="mailto:privacy@getmega.com">privacy@getmega.com</a>
      </p>
      <BottomNav />
      <NavIcons />
      <Footer />
    </div>
  </Layout>
);

export default PrivacyPolicy;
