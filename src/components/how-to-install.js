import React from "react";
import HowToImages from "./how-to-images";

const HowToInstall = () => {
  return (
    <>
      <div className="homepage how-to-install">
        <div className="install-text">HOW TO INSTALL?</div>
        <HowToImages />
      </div>
    </>
  );
};

export default HowToInstall;
