import React from "react";
import CarouselItems from "./carousel-items";
import "../styles/_carousel.scss";

const Carousel = () => {
  return (
    <div className="carousel">
      <input type="radio" id="carousel-1" name="carousel[]" defaultChecked />
      <input type="radio" id="carousel-2" name="carousel[]" />
      <input type="radio" id="carousel-3" name="carousel[]" />
      <input type="radio" id="carousel-4" name="carousel[]" />
      <ul className="carousel__items">
        <CarouselItems />
      </ul>
      <div className="carousel__prev">
        <label htmlFor="carousel-1" />
        <label htmlFor="carousel-2" />
        <label htmlFor="carousel-3" />
        <label htmlFor="carousel-4" />
      </div>
      <div className="carousel__next">
        <label htmlFor="carousel-1" />
        <label htmlFor="carousel-2" />
        <label htmlFor="carousel-3" />
        <label htmlFor="carousel-4" />
      </div>
      <div className="carousel__nav">
        {/* <label htmlFor="carousel-1" />
        <label htmlFor="carousel-2" />
        <label htmlFor="carousel-3" />
        <label htmlFor="carousel-4" /> */}
      </div>
    </div>
  );
};

export default Carousel;
