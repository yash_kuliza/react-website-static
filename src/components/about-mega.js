import React from "react";
import AboutMegaLinks from "./about-mega-links";

const AboutMega = () => {
  return (
    <>
      <div className="homepage about-section">
        <div className="about-mega-header">About Mega</div>
        <div className="external-links">
          <ul>
            <AboutMegaLinks />
          </ul>
        </div>
        <div className="homepage hide-for-mobile">
          <div className="disclaimer">
            <div className="disclaimer-header">DISCLAIMER</div>
            <p>
              is the owner of, and reserves all rights to the assets, content,
              services, information, and products and graphics in the website
              except any third party content.
            </p>
            <p>
              refuses to acknowledge or represent about the accuracy or
              completeness or reliability or adequacy of the website's third
              party content. These content, materials, information, services,
              and products in this website, including text, graphics, and links,
              are provided "AS IS" and without warranties of any kind, whether
              expressed or implied.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default AboutMega;
