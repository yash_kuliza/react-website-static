import React from "react";
import FeaturesContent from "./features-content";

const Features = () => {
  return (
    <>
      <div className="homepage features">
        <div className="features-header">
          <p>JUMP. THINK.</p> <p>LOOP. WIN</p>
          <p className="sub-text">Loser says what?</p>
        </div>
        <FeaturesContent />
      </div>
    </>
  );
};

export default Features;
