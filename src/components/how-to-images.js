import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

const Images = graphql`
  query {
    image1: file(relativePath: { eq: "howto_image1.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    image2: file(relativePath: { eq: "howto_image2.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    image3: file(relativePath: { eq: "howto_image3.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    image4: file(relativePath: { eq: "howto_image4.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

const HowToImages = () => {
  return (
    <StaticQuery
      query={Images}
      render={data => (
        <>
          <div className="install-img-container ">
            <div className="install-images">
              <div>
                <Img fluid={data.image1.childImageSharp.fluid} alt="" />{" "}
              </div>
              <div>
                <Img fluid={data.image2.childImageSharp.fluid} alt="" />{" "}
              </div>
              <div>
                <Img fluid={data.image3.childImageSharp.fluid} alt="" />{" "}
              </div>
              <div>
                <Img fluid={data.image4.childImageSharp.fluid} alt="" />{" "}
              </div>
            </div>
          </div>
        </>
      )}
    />
  );
};

export default HowToImages;
