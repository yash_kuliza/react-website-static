import React from "react";
import { Link } from "gatsby";
import NavIcons from "./nav-icons";

const items = [
  // { link: "#about", text: "About" },
  // { link: "#faq", text: "FAQs" }
];

const NavItems = () => {
  return (
    <>
      <ul className="nav">
        {items.map((obj, key) => (
          <li key={key}>
            <Link to={`/${obj.link}`}>{obj.text}</Link>
          </li>
        ))}
      </ul>
      <NavIcons />
    </>
  );
};

export default NavItems;
