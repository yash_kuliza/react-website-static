import React from "react";
import Logo from "./header";
import Links from "./about-mega-links";

const BottomNav = () => {
  return (
    <>
      <div className="homepage footer-logo">
        <Logo />
      </div>
      <div className="homepage about-section">
        <div className="external-links">
          <ul>
            <Links />
          </ul>
          <hr />
        </div>
      </div>
    </>
  );
};

export default BottomNav;
