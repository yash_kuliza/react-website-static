import React from "react";
import PropTypes from "prop-types";
import "../styles/layout.scss";
import "../styles/home.scss";
import "../styles/fonts.scss";

const Layout = ({ children }) => (
  <>
    <div>
      <main>{children}</main>
    </div>
  </>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
