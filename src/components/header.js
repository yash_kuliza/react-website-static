import { StaticQuery, graphql, Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";
import Img from "gatsby-image";

const Logo = graphql`
  query {
    placeholderImage: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        fluid(maxWidth: 164) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;

const Header = ({ siteTitle }) => (
  <header>
    <StaticQuery
      query={Logo}
      render={data => (
        <Link to="/">
          <Img
            className="logo"
            fluid={data.placeholderImage.childImageSharp.fluid}
            alt="logo"
          />
        </Link>
      )}
    />
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string
};

Header.defaultProps = {
  siteTitle: ``
};

export default Header;
