import React from "react";

const Footer = () => {
  return (
    <>
      <footer className="footer-container ">
        <div className="content">
          Copyright © 2019 - Mega Games{" "}
          <span className="hide-for-mobile">|</span> All rights reserved
        </div>
      </footer>
    </>
  );
};

export default Footer;
