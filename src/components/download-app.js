import React from "react";

const DownloadApp = () => {
  return (
    <>
      <div className="download-button hide-for-desktop">
        <a id="appLinkDownload" href="/">
          Download & Get ₹10
        </a>
      </div>
    </>
  );
};

export default DownloadApp;
