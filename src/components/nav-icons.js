import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

const icons = graphql`
  query {
    icon1: file(relativePath: { eq: "icn-whatsapp.png" }) {
      childImageSharp {
        fluid(maxWidth: 164) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    icon2: file(relativePath: { eq: "icn-facebook.png" }) {
      childImageSharp {
        fluid(maxWidth: 164) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    icon3: file(relativePath: { eq: "icn-insta.png" }) {
      childImageSharp {
        fluid(maxWidth: 164) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`;

const NavIcons = () => {
  return (
    <ul className="icons">
      <StaticQuery
        query={icons}
        render={data => (
          <>
            <li>
              <Img fluid={data.icon1.childImageSharp.fluid} alt="" />
            </li>
            <li>
              <Img
                className="fb"
                fluid={data.icon2.childImageSharp.fluid}
                alt=""
              />
            </li>
            <li>
              <Img fluid={data.icon3.childImageSharp.fluid} alt="" />{" "}
            </li>
          </>
        )}
      />
    </ul>
  );
};

export default NavIcons;
