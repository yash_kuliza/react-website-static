import React from "react";
import Header from "./header";
import Links from "./about-mega-links";
import "../styles/_hamburger_menu.scss";

const MobileMenu = () => {
  return (
    <div className="mobile-nav hide-for-desktop">
      <Header />
      <input id="burger" type="checkbox" />

      <label htmlFor="burger">
        <span />
        <span />
        <span />
      </label>

      <nav>
        <ul>
          {/* <li>
            <a href="#">Link #1</a>
          </li>
          <li>
            <a href="#">Link #2</a>
          </li>
          <li>
            <a href="#">Link #3</a>
          </li> */}
          <Links />
        </ul>
      </nav>
    </div>
  );
};

export default MobileMenu;
