import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

const Items = graphql`
  query {
    image1: file(relativePath: { eq: "introimage1.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image2: file(relativePath: { eq: "introimage1.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image3: file(relativePath: { eq: "introimage1.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image4: file(relativePath: { eq: "introimage1.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;

const CarouselItems = () => {
  return (
    <StaticQuery
      query={Items}
      render={data => (
        <>
          <li className="carousel__item">
            <Img fluid={data.image1.childImageSharp.fluid} alt="" />

            <div className="item-box">
              <p className="item-text">
                War is coming to outer space. It is your job to restore peace
                and prosperity to galaxy X and defeat the Tyrannical Empire.
                Only the true heroes will prevail from this bullet hell.
              </p>
              <span className="item-likes">20K</span>
              <span className="liked">Liked</span>
            </div>
          </li>
          <li className="carousel__item">
            <Img fluid={data.image2.childImageSharp.fluid} alt="" />

            <div className="item-box">
              <p className="item-text">
                War is coming to outer space. It is your job to restore peace
                and prosperity to galaxy X and defeat the Tyrannical Empire.
                Only the true heroes will prevail from this bullet hell.{" "}
              </p>
              <span className="item-likes">30K</span>
              <span className="liked">Liked</span>
            </div>
          </li>
          <li className="carousel__item">
            <Img fluid={data.image3.childImageSharp.fluid} alt="" />{" "}
            <div className="item-box">
              <p className="item-text">
                War is coming to outer space. It is your job to restore peace
                and prosperity to galaxy X and defeat the Tyrannical Empire.
                Only the true heroes will prevail from this bullet hell.
              </p>
              <span className="item-likes">40K</span>
              <span className="liked">Liked</span>
            </div>
          </li>
          <li className="carousel__item">
            <Img fluid={data.image4.childImageSharp.fluid} alt="" />{" "}
            <div className="item-box">
              <p className="item-text">
                War is coming to outer space. It is your job to restore peace
                and prosperity to galaxy X and defeat the Tyrannical Empire.
                Only the true heroes will prevail from this bullet hell.
              </p>
              <span className="item-likes">50K</span>
              <span className="liked">Liked</span>
            </div>
          </li>
        </>
      )}
    />
  );
};

export default CarouselItems;
