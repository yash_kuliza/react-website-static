import React from "react";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image";

const Images = graphql`
  query {
    image1: file(relativePath: { eq: "Icn_Quick.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image2: file(relativePath: { eq: "Icn_Skills.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image3: file(relativePath: { eq: "Icn_Money.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    image4: file(relativePath: { eq: "Icn_Trophy.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
`;

const FeaturesContent = () => {
  return (
    <StaticQuery
      query={Images}
      render={data => (
        <>
          <div className="features-img-container ">
            <div className="features-images">
              <div>
                <Img fluid={data.image1.childImageSharp.fluid} alt="quick" />{" "}
                <p className="feature-heading">Quick, Easy Games</p>
                <p className="feature-text">May still be hard for you.</p>
              </div>
              <div>
                <Img fluid={data.image2.childImageSharp.fluid} alt="skills" />{" "}
                <p className="feature-heading">Skills Galore</p>
                <p className="feature-text">
                  Cognitive, problem-solving, decision-making. <br />
                  People pleasing not included.
                </p>
              </div>
              <div>
                <Img fluid={data.image3.childImageSharp.fluid} alt="money" />{" "}
                <p className="feature-heading">Here comes the money</p>
                <p className="feature-text">
                  Only for those who win. Don’t <br />
                  get your hopes up.
                </p>
              </div>
              <div>
                <Img fluid={data.image4.childImageSharp.fluid} alt="trophy" />{" "}
                <p className="feature-heading">Hoot for the Loot</p>
                <p className="feature-text">
                  Faster than you can say…well,
                  <br /> pretty much anything.
                </p>
              </div>
            </div>
          </div>
        </>
      )}
    />
  );
};

export default FeaturesContent;
