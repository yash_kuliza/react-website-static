import React from "react";
import { Link } from "gatsby";

const Links = [
  { link: "terms", text: "Terms & Conditions" },
  // { link: "#gamesofskill", text: "Games of Skill" },
  { link: "privacy-policy", text: "Privacy Policy" }
  // { link: "#refunds", text: "Returns, Refunds & Cancellation" },
  // { link: "#about", text: "About" },
  // { link: "#faq", text: "FAQs" },
  // { link: "#help", text: "Help & Support" }
];

const AboutMegaLinks = () => {
  return (
    <>
      {Links.map((obj, key) => (
        <li key={key}>
          <Link to={`/${obj.link}`}>{obj.text}</Link>
        </li>
      ))}
    </>
  );
};

export default AboutMegaLinks;
