import React from "react";
import Carousel from "./carousel";
import Header from "./header";
import NavItems from "./nav-items";
import MobileNav from "./mobile-menu";

class Home extends React.Component {
  submit = e => {
    e.preventDefault();
  };
  render() {
    return (
      <>
        <div className="homepage">
          <div className="header">
            <Header />
            <NavItems />
            <MobileNav />
          </div>
          <div className="content">
            <div className="content-left">
              <div className="play-game-container">
                <div className="play-games">
                  <p>THINK YOU ARE THE BEST?</p>
                </div>
                <div className="earn-money">
                  Time to burst that bubble. Game mode on!
                </div>
              </div>
              <div className="form-container hide-for-mobile">
                {/* <form name="downloadApp" onSubmit={this.submit}>
                  <div>
                    <div className="input-group prefix">
                      <span className="input-group-addon">+91</span>
                      <input
                        id="phoneNumber"
                        type="text"
                        name="phone"
                        placeholder="Phone number"
                      />
                    </div>
                    <div id="input-error" className="error hide">
                      Error test
                    </div>
                  </div>
                  <div id="loading-spinner" className="hide">
                    <div className="loader" />
                  </div>

                  <div id="submit-btn">
                    <input
                      className="submit-button"
                      type="submit"
                      value="Get App Link SMS"
                    />
                    <br />
                  </div>
                  <div id="success-btn" className="hide">
                    <span>
                      {" "}
                      <img src="/img/icons/checked.png" alt="" />
                    </span>
                    <span className="send-again">Send Again</span>
                  </div>
                </form> */}
              </div>
            </div>
            <div className="carousel-container">
              <Carousel />
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Home;
